#include "Array.h"
#include <stdexcept>
#include <iostream>

using namespace std;

Array::Array(int count) : pointer()
{
	this->setCount(count);
	this->pointer = allocateMemory(this->getCount());
}

Array::Array(int start, int count)
{
	this->setCount(count);
	this->pointer = allocateMemory(this->getCount());
	for (int i = 0; i < this->getCount(); i++)
	{
		this->pointer[i] = start + i;
	}
}

int Array::getCount() const
{
	return this->count;
}

int Array::getSum()
{
	int sum = 0;

	for (int i = 0; i < this->getCount(); i++) {
		sum += this->getElement(i);
	}

	return sum;
}

int Array::getElement(int index)
{
	if (index < 0 || index > this->getCount() - 1)
	{
		throw std::out_of_range("n is invalid");
	}

	return this->pointer[index];
}

void Array::setElement(int index, int value)
{
	if (index < 0 || index > this->getCount() - 1)
	{
		throw std::out_of_range("n is invalid");
	}

	this->pointer[index] = value;
}

//void Array::displayArray()
//{
//	for (int i = 0; i < this->getCount(); i++)
//	{
//		cout << this->pointer[i] << " ";
//	}
//}

void Array::swap(int index1, int index2)
{
	int t = this->getElement(index1);
	this->setElement(index1, this->getElement(index2));
	this->setElement(index2, t);
}

void Array::reverseArray()
{
	for (int i = 0; i < this->getCount() / 2; i++)
	{
		swap(this->getElement(i), this->getElement(this->getCount() - 1 - i));
	}
}

void Array::shiftArrayByOne()
{
	int t = this->getElement(1);
	this->setElement(1, this->getElement(0));

	for (int i = 1; i < this->getCount(); i++)
	{
		if (i == this->getCount() - 1) {
			this->setElement(0, this->getElement(i));
		}

		t = this->getElement(i + 1);
		this->setElement(i + 1, t);
	}
}

void Array::randomInput()
{
	srand(time(0));

	for (int i = 0; i < this->getCount(); i++) {
		int num = 1 + rand() % 100;
		this->setElement(i, num);
	}
}

void Array::sort(comparer comparer)
{
	bool flag = false;

	for (int i = 0; i < this->getCount(); i++) {
		for (int j = 0; j < this->getCount() - 1; j++)
		{
			if (comparer(this->getElement(i), this->getElement(i + 1)))
			{
				swap(this->getElement(i + 1), this->getElement(i));
			}
		}
	}
}


Array Array::select(predicate predicate)
{
	if (predicate == nullptr)
	{
		throw std::invalid_argument("predicate is nullptr");
	}

	int* array = allocateMemory(this->getCount());
	int k = 0;

	for (int i = 0; i < this->getCount(); i++)
	{
		if (predicate(this->getElement(i)))
		{
			array[k++] = this->getElement(i);
		}
	}

	Array result(k);

	for (int i = 0; i < result.getCount(); i++)
	{
		result.setElement(i, array[i]);
	}

	delete[] array;

	return result;
}

//Array::~Array()
//{
//	delete[] this->pointer;
//}

void Array::setCount(int count)
{
	if (count < 1)
	{
		throw std::out_of_range("n is invalid");
	}

	this->count = count;
}

int* Array::allocateMemory(int n)
{
	int *p = new int[n];

	for (int i = 0; i < n; i++)
	{
		p[i] = 0;
	}

	return p;
}
