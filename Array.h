#pragma once

typedef int(*comparer)(int, int);
typedef bool(*predicate)(int);

class Array
{
public:
	Array(int);
	Array(int, int);
	int getCount() const;
	int getElement(int);
	int getSum();
	void setElement(int, int);
	void sort(comparer);
	void reverseArray();
	void randomInput();
	void shiftArrayByOne();
//	void displayArray();
	Array select(predicate);
//	~Array();
private: 
	int* pointer = nullptr;
	int count = 0;
	void swap(int, int);
	void setCount(int);
	static int* allocateMemory(int);
};