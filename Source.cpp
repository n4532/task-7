#include <iostream>
#include "Array.h"

using namespace std;

bool odd(int value);
bool even(int value);

int main()
{
	Array array(7, 12);

	for (int i = 0; i < array.getCount(); i++)
	{
		cout << array.getElement(i) << " ";
	}

	cout << endl;
	array.setElement(5, 56);

	Array array1 = array.select(odd);

	for (int i = 0; i < array1.getCount(); i++)
	{
		cout << array1.getElement(i) << " ";
	}

	cout << endl;

	Array array2 = array.select(even);

	for (int i = 0; i < array2.getCount(); i++)
	{
		cout << array2.getElement(i) << " ";
	}
}

bool odd(int value)
{
	return value % 2;
}

bool even(int value)
{
	return !(value % 2);
}